# What's this project about?
A simple script that updates and cleans caches, why cause im lazy and dont want to type serveral commands when updating my linux systems.
The script itself is self-updating so once its installed it checks for updates upon launch it also cleans up old kernels and user trash dirs.

## What platforms is this intended for
Most major linux platforms that are out there its self-updating so once it lauches it checks for a new version using curl and ca-certificates

### Specific Hardware Support

* Raspberry Pi 
* Cubieboard

### Specific Platform Support

* Debian based
* Ubuntu Based

### Additional Platform Updaters

* Python
* Nodejs
* Google Cloudplatform
* Ruby

### Additional Platform Support

* Dietpi
* OSMC

### Download

[Download zip](https://gitlab.com/spitfire-project/the-updatr/repository/archive.zip)

[Download tar.gz](https://gitlab.com/spitfire-project/the-updatr/repository/archive.tar.gz)